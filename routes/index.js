const express = require('express');

const router = express.Router();

// http://localhost:3000/ping
router.get('/ping', (req, res) => {
  res.status(200).json('Server working properly!');
});

module.exports = router;
