/* eslint-disable no-underscore-dangle, no-param-reassign */
const mongoose = require('mongoose');

const { Schema, model } = mongoose;

const authorSchema = new Schema(
  {
    name: { type: String, required: true, lowercase: true, trim: true },
    surname: { type: String, lowercase: true, trim: true },
  },
  {
    toJSON: {
      transform: (doc, ret) => {
        ret.id = doc._id;
        delete ret._id;
        delete ret.__v;
        return ret;
      },
      virtuals: true,
    },
  },
);

authorSchema.virtual('songs', {
  ref: 'Song',
  localField: '_id',
  foreignField: 'authorId',
});

const Author = model('Author', authorSchema);
module.exports = Author;
