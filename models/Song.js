/* eslint-disable no-underscore-dangle, no-param-reassign */
const mongoose = require('mongoose');

const { Schema } = mongoose;

const songsSchema = new Schema(
  {
    title: { type: String, required: true, trim: true, lowercase: true },
    authorId: { type: Schema.Types.ObjectId, ref: 'Author' },
    // @TODO Change the model to support future years. I'm sorry.
    year: { type: Number, min: 0, max: new Date().getFullYear() },
  },
  {
    toJSON: {
      transform: (doc, ret) => {
        ret.id = doc._id;
        delete ret._id;
        delete ret.__v;
        return ret;
      },
    },
  },
);

const Song = mongoose.model('Song', songsSchema);
module.exports = Song;
